/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iteraciones;

/**
 *
 * @author a_lej
 */
public class Iteraciones {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*
        // Invrementar en 1 el valor numérico
        int a = 0;
        a = a + 1;
        a += 1;
        a++;
        
        String saludo = "hola fkashfd asdkfsafas";
        // obtener posicion de la cadena
        System.out.println(saludo.charAt(3));
        // traer el tamaño de la cadena
        System.out.println(saludo.length());
        // traer una porción de la cadena
        System.out.println(saludo.substring(0, 4)); */
        
        /*
        // WHILE
        int i = 0;
        while(i <= 10) {
            System.out.println(i);
            i++;
        } */
        
        /* String saludo = "Hola";
        int i = 0;
        do {
            System.out.println(saludo);
        } while (i == 5); */
        
        /*
        // FOR
        for(int i = 0; i < 15; i++) {
            System.out.println("Se esta cumpliendo la condicion!");
        } */
        
        // ejemplo 1
        /* for (int i = 1; i <= 1000; i++) {
            if (i%4 == 0 && i%7 == 0) {
                System.out.println(i + " es multiplo de 4 y 7");
            }
        } */
        
        /*
        // iteracion de 4 en 4
        for (int i = 4; i <= 100; i += 4) {
            if (i % 7 == 0) {
             System.out.println(i);   
            }
        } */
        
        /*
        // iteracion de 7 en 7
        for (int i = 7; i <= 100; i+= 7) {
            if (i % 4 == 0) {
                System.out.println(i);
            }
        } */
        
        
        /*
        // potencia WHILE
        int base = 2;
        int potencia = 3;
        int i = 0;
        int res = 1;
        while(i < potencia){
            res *= base;
            i++;
        }
        System.out.println(res);*/
        
        // potencia FOR
        int base = 2;
        int potencia = 3;
        int res = 1;
        for(int i = 0; i < potencia; i++) {
            res *= base;
        }
        System.out.println(res);
    }
    
}
